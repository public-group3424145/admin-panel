import ProductCardToEdit from '../../components/product-card-to-edit';
import ProductCard from '../../components/product-card';
import api from '../../api';

import { useOutletContext } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { Grid, Box } from '@mui/material';
import React, { useEffect } from 'react';

function ProductPage() {
  const { id } = useParams();

  const {
    handleDelete,
    editCardId,
    cardToEdit,
    handleFullPageFormSubmit,
    card,
    setCard,
    loading,
    setLoading,
  } = useOutletContext();

  useEffect(() => {
    const fetchCardById = async () => {
      try {
        const response = await api.get(`/${id}`);

        if (response.data.success === true) {
          setCard(response.data.data);
        }
        setLoading(false);
      } catch (error) {
        console.error('Error fetching card:', error);
        setLoading(false);
      }
    };

    fetchCardById();
  }, [id]);

  if (loading) {
    return (
      <Box
        sx={{
          margin: 0,
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        Loading...
      </Box>
    );
  }

  return (
    <Grid container spacing={2} position="relative">
      <Grid item xs={2}></Grid>
      {card !== null && (
        <Grid item xs={8} mt={3}>
          {editCardId === card.id ? (
            <ProductCardToEdit
              data={card}
              isArrowShown={false}
              isDeleteiconShown={false}
              defaultValue={editCardId !== null && cardToEdit}
              handleFormSubmit={handleFullPageFormSubmit}
            />
          ) : (
            <ProductCard
              data={card}
              isArrowShown={false}
              handleDelete={handleDelete}
              isDeleteiconShown={false}
            />
          )}
        </Grid>
      )}
    </Grid>
  );
}

export default ProductPage;
