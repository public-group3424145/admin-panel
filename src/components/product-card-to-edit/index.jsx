import EditableField from '../editable-field';

import React, { useState } from 'react';
import {
  Card,
  CardContent,
  Typography,
  Divider,
  Box,
  IconButton,
} from '@mui/material';
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { Link as MuiLink } from '@mui/material';
import { Link, useOutletContext } from 'react-router-dom';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import CardMedia from '@mui/material/CardMedia';
import DefaultImage from '../../images/coming-soon.jpg';

function ProductCardToEdit({
  data,
  isArrowShown,
  isDeleteiconShown,
  defaultValue,
  handleFormSubmit,
}) {
  const { handleDelete } = useOutletContext();

  const [formState, setFormState] = useState(
    defaultValue || {
      name: '',
      description: '',
      quantity: '',
      price: '',
      id: data.id,
    },
  );

  const handleChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <Box
      component="form"
      noValidate
      autoComplete="off"
      onSubmit={(event) => handleFormSubmit(event, formState, formState.id)}
    >
      <Card sx={{ minHeight: '180px' }}>
        <CardContent>
          {isDeleteiconShown && (
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}
              mb={1}
            >
              <IconButton
                sx={{ padding: 0 }}
                onClick={() => handleDelete(data.id)}
              >
                <DeleteOutlineIcon fontSize="small" />
              </IconButton>
            </Box>
          )}

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <EditableField
              formState={formState}
              fieldName="name"
              handleChange={handleChange}
              data={data}
              formValue={formState.name}
              defaultValue={defaultValue.name}
            />

            <EditableField
              formState={formState}
              fieldName="description"
              handleChange={handleChange}
              data={data}
              formValue={formState.description}
              defaultValue={defaultValue.description}
            />

            <CardMedia
              sx={{ height: 115, width: 140 }}
              image={DefaultImage}
              title="pic"
            />
          </Box>

          <Divider sx={{ margin: '20px auto' }} />

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              textAlign: 'center',
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}
          >
            <Typography
              gutterBottom
              component="span"
              sx={{
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
              }}
            >
              {`quantity: ${data.quantity}`}
            </Typography>

            <Typography
              component="span"
              sx={{
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
              }}
            >{`price: ${data.price}`}</Typography>
          </Box>

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-end',
            }}
          >
            {isArrowShown && (
              <MuiLink
                component={Link}
                target="_blank"
                rel="noopener noreferrer"
                to={`/${data.id}`}
                underline="none"
                sx={{ cursor: 'ponter', marginTop: '20px' }}
              >
                <ArrowCircleRightOutlinedIcon />
              </MuiLink>
            )}
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
}

export default ProductCardToEdit;
