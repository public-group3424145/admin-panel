import ProductList from './components/product-list';
import ProductPage from './pages/product-page';
import Main from './pages/main';
import {
  RouterProvider,
  createBrowserRouter,
  createHashRouter,
} from 'react-router-dom';

function App() {
  const router = createBrowserRouter([
    {
      element: <Main />,
      children: [
        { path: '/', element: <ProductList /> },
        { path: '/:id', element: <ProductPage /> },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
